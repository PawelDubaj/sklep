const express = require('express');
const router = express.Router();
const Produkt = require('./../models/produkt');



router.get('/:szukaj', (req, res) => {
    Produkt.find({
        nazwa: {
            '$regex': new RegExp(req.params.szukaj, 'i')
        }
    }, (err, item) => {
        
        if (err) throw err;

        res.json(item);
    })
})


module.exports = router;