const express = require('express');
const router = express.Router();
const Produkt = require('./../models/produkt');

// router.get('/', (req, res) => {  // wszystkie 
//     Produkt.find({}, {_id: 1, nazwa: 1}, (err, items) => {
//         if(err) throw err;

//         res.json(items);
//     });
// });

// router.get('/', (req, res) => { // pierwsze 6
//     Produkt.find({}, {}, { limit: 6 }, (err, item) => {
//         if(err) throw err;

//         res.json(item);
//     });
// });

router.get('/', (req, res) => {
    let query = {};
    let options = {
        limit: 6
    };
   

    // filtrowanie po:
    // po kategorie //?kat=1234234
    // po podkategoriach // ?pod=3423423
    // po cenie //?c=1&c=4  // [1,4]
    // po marce // ?m=gafs
    // po wieku // ?w==12&w=34 // [12,24]

    // ?kat=123123&pod=12312  // filtr po kilku wariantach

     // Object.keys(req.query);
    //  console.log( Object.keys(req.query));

    if(req.query.kat) {
        query.kategoria = req.query.kat;
    }

    if(req.query.pod) {
        query.podkategoria = req.query.pod;
    }

    if(req.query.c && req.query.c.length == 2) {
        query.cena = {
            '$gte': req.query.c[0],
            '$lte': req.query.c[1]
        }
    }

    if(req.query.w && req.query.w.length == 2) {
        query.wiek = {
            '$gte': req.query.w[0],
            '$lte': req.query.w[1]
        }
    }

    if(req.query.m) {
        query.marka = {
            '$in': req.query.m
        }
    }

    console.log(query);

     if(Object.keys(req,query).length > 0) {
         delete options.limit;
     }

    Produkt.find(query, {},  options, (err, item) => {
        if(err) throw err;

        res.json(item);
    });
});

// router.get('/', (req, res) => {
//     Produkt.find({}, {}, { limit: 6 }, (err, item) => {
//         if(err) throw err;

//         res.json(item);
//     });
// });

router.get('/:_id', (req, res) => {  // po id
    Produkt.findOne({_id: req.params._id }, (err, item) => {
        if(err) throw err;

        res.json(item);
    });
});

router.get('/:nazwa', (req, res) => {  // po id
    Produkt.findOne({ nazwa: req.params.nazwa }, (err, item) => {
        if(err) throw err;

        res.json(item);
    });
});

// router.post('/newprodukt', (req, res) => {
//     var produkt = new Produkt(
//         nazwa = req.body.nazwa,
//         cena = req.body.cena,
//         marka = req.body.marka,
//         opis = req.body.opis,
//         wiek = req.body.wiek,
//         img_small = req.body.img_small,
//         img_big = req.body.img_big,
//         kategoria = req.body.kategoria,
//         podkategoria = req.body.podkategoria
//     );
//     Produkt.save((err, rodukt) => {
//         if(err) throw err;
//     });
// });

module.exports = router;