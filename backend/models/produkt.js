const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProduktSchema = new Schema({
    nazwa: {
        type: String,
        required: true
    },
    cena: {
        type: Number,
        required: true
    },
    marka: {
        type: String,
        required: true
    },
    opis: {
        type: String,
        required: true
    },
    wiek: {
        type: Number,
        required: true
    },
    img_small: {
        type: String,
        required: true
    },
    img_big: {
        type: String,
        required: true
    },
    kategoria: {
        type: Schema.Types.ObjectId,    // w mongodb id ma swój własny typ
        ref: 'Kategoria', //module.exports = mongoose.model('Kategoria', KategoriaSchema);
        required: true
    },
    podkategoria: {
        type: Schema.Types.ObjectId,
        ref: 'Kategoria.prodkategorie',  //podkategorie: [] z kategoria.js
        required: true
    }
}, { collection: 'produkty'});

module.exports = mongoose.model('Produkt', ProduktSchema);
