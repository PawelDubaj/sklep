import { TestBed, inject } from '@angular/core/testing';

import { KategoriaService } from './kategoria.service';

describe('KategoriaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KategoriaService]
    });
  });

  it('should be created', inject([KategoriaService], (service: KategoriaService) => {
    expect(service).toBeTruthy();
  }));
});
