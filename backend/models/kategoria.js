const mongoose = require('mongoose');
const Schema = mongoose.Schema; //jest również konstruktorem
const slug = require('slug'); 

const KategoriaSchema = new Schema({
    nazwa: {
        type: String,
        required: true, 
    },
    slug:  {
        type: String,
        unique: true
    },
    podkategorie: [
        {
            nazwa: {
                type: String,
                required: true
            }
        }
    ]
}, { collection: 'kategorie' });

KategoriaSchema.pre('save', function(next) {
    this.slug = slug(this.nazwa);
    next();
});

module.exports = mongoose.model('Kategoria', KategoriaSchema);

// aa: [],
// bb: [{}],
// cc: [[String]],
// dd: [String],
// ee: [Number],
// ff: [[{}]],
// gg: [[String],[Number]],